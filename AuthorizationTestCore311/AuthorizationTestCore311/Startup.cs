using AuthorizationTest.Database;
using AuthorizationTest.Models;
using AuthorizationTest.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationTestCore311
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ApplicationUser, UserViewModel>()
                    .ForMember(dest => dest.IsianUsername, act => act.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.IsianEmail, act => act.MapFrom(src => src.Email));
            }));

            services.AddDbContext<ApplicationDbContext>(option =>
            {
                option.UseNpgsql("Host=localhost;Database=authorization_test_311;Username=postgres;Password=mashudahP");
            });

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_.";

                options.Password.RequiredLength = 8;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequiredUniqueChars = 1;
            })
            .AddDefaultTokenProviders()
            .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Bearer";
                options.DefaultChallengeScheme = "Bearer";
                options.DefaultScheme = "Bearer";
            }).AddJwtBearer(options =>
            {
                options.IncludeErrorDetails = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    // ValidAlgorithms = ["HS256"],

                    ValidIssuer = "auth",
                    ValidAudience = "auth",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("2b99ed46-cf9e-4795-a4a5-0b26cc15e3f3"))
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            RegisterRoles(app).GetAwaiter().GetResult();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void RegisterPolicies(AuthorizationOptions options)
        {
            var claims = new string[] {
                    "project.view", "project.create", "project.edit", "project.delete",
                    "post.view", "post.create", "post.edit", "post.delete"
                };

            foreach (var claim in claims)
            {
                options.AddPolicy(claim, policy => policy.RequireClaim(claim));
            }
        }

        private static async Task RegisterRoles(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

                var admin = await userManager.FindByNameAsync("admin");
                if (admin == null)
                {
                    var result = await userManager.CreateAsync(new ApplicationUser { UserName = "admin", Email = "admin@bff.com" }, "Aaa123???");

                    admin = await userManager.FindByNameAsync("admin");
                }

                var roles = new string[] { "admin", "editor", "member", "guest" };

                foreach (var role in roles)
                {
                    if (!await roleManager.RoleExistsAsync(role))
                    {
                        await roleManager.CreateAsync(new ApplicationRole { Name = role });
                    }
                }

                var claims = new string[] {
                    "project.view", "project.create", "project.edit", "project.delete",
                    "post.view", "post.create", "post.edit", "post.delete"
                };

                var adminRole = await roleManager.FindByNameAsync("admin");

                var existedClaims = await roleManager.GetClaimsAsync(adminRole);

                foreach (var claim in claims)
                {
                    if (existedClaims.FirstOrDefault(x => x.Type == "permission" && x.Value == claim) == null)
                    {
                        await roleManager.AddClaimAsync(adminRole, new Claim("permission", claim));
                    }
                }

                await userManager.AddToRoleAsync(admin, "admin");
            }
        }
    }
}
