﻿using System.ComponentModel.DataAnnotations;

namespace AuthorizationTest.Models
{
    public class RolePermission
    {
        [Key]
        [Required]
        [MaxLength(32)]
        public string Name { get; set; } // CanReadGTA, CanWriteGTA, CanEditGTA

        [Required]
        public string Description { get; set; }
    }
}
