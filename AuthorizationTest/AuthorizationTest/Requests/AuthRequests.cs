﻿using System.ComponentModel.DataAnnotations;

namespace AuthorizationTest.Requests
{
    public class RequestAuthRegister
    {
        [Required]
        [EmailAddress]
        [MaxLength(255)]
        public string Email { get => _email; set => _email = value.Trim(); }
        private string _email;

        [Required]
        [MaxLength(60)]
        public string Username { get => _username; set => _username = value.Trim(); }
        private string _username;

        [Required]
        [MaxLength(255)]
        public string Password { get => _password; set => _password = value.Trim(); }
        private string _password;
    }

    public class RequestAuthLoginByEmail
    {
        [Required]
        [EmailAddress]
        [MaxLength(255)]
        public string Email { get => _email; set => _email = value.Trim(); }
        private string _email;

        [Required]
        [MaxLength(255)]
        public string Password { get => _password; set => _password = value.Trim(); }
        private string _password;
    }
}
