﻿using Microsoft.AspNetCore.Mvc;

namespace AuthorizationTest.Responses
{
    public enum ResponseStatus
    {
        Success, Error
    }

    public class BaseResponse
    {
        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
        public object? Data { get; set; }

        public object Json()
        {
            return new
            {
                status = Status.ToString().ToLower(),
                message = Message,
                data = Data
            };
        }

        public static BaseResponse Success(string message, object? data = null)
        {
            return new BaseResponse
            {
                Status = ResponseStatus.Success,
                Message = message,
                Data = data
            };
        }

        public static BaseResponse Error(string message, object? data = null)
        {
            return new BaseResponse
            {
                Status = ResponseStatus.Error,
                Message = message,
                Data = data
            };
        }

        public static BaseResponse ExceptionError(Exception err)
        {
            return new BaseResponse
            {
                Status = ResponseStatus.Error,
                Message = err.Message,
                Data = null
            };
        }

        public static BaseResponse UnknownError(Exception err = null)
        {
            return new BaseResponse
            {
                Status = ResponseStatus.Error,
                Message = "Unknown Error",
                Data = null
            };
        }
    }
}
