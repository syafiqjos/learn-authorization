﻿using Microsoft.AspNetCore.Mvc;

namespace AuthorizationTest.Responses
{
    public class JsonResponse
    {
        public static object Success(string message, object? data = null)
        {
            return BaseResponse.Success(message, data).Json();
        }

        public static object Error(string message, object? data = null)
        {
            return BaseResponse.Error(message, data).Json();
        }

        public static object ExceptionError(Exception err)
        {
            return BaseResponse.ExceptionError(err).Json();
        }

        public static object UnknownError(Exception err = null)
        {
            return BaseResponse.UnknownError(err).Json();
        }
    }
}
