﻿using AuthorizationTest.Database;

namespace AuthorizationTest.Services
{
    public class BaseRepository
    {
        protected ApplicationDbContext context;

        public BaseRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
    }
}
