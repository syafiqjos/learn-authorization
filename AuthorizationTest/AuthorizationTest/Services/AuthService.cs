﻿using AuthorizationTest.Configurations;
using AuthorizationTest.Database;
using AuthorizationTest.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace AuthorizationTest.Services
{
    public class AuthService : BaseRepository
    {
        private readonly string[] PasswordStrengthExceptions = { "PasswordTooShort", "PasswordRequiresNonAlphanumeric", "PasswordRequiresDigit", "PasswordRequiresUpper" };

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationUser> _roleManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly JwtSettings _jwtSettings;

        private readonly string _passwordHashSalt;
        private readonly string _resetPasswordTokenSecret;
        private readonly string _emailConfirmationTokenSecret;

        public AuthService(
            ApplicationDbContext context,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationUser> roleManager,
            IAuthorizationService authorizationService,
            JwtSettings jwtSettings,

            string passwordHashSalt = "",
            string resetPasswordTokenSecret = "",
            string emailConfirmationTokenSecret = ""
        ) : base(context)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _authorizationService = authorizationService;
            _jwtSettings = jwtSettings;

            _passwordHashSalt = passwordHashSalt;
            _resetPasswordTokenSecret = resetPasswordTokenSecret;
            _emailConfirmationTokenSecret = emailConfirmationTokenSecret;
        }

        public async Task<ApplicationUser> SignIn(ApplicationUser user)
        {
            if (user == null) return null;

            await _signInManager.SignInAsync(user, false);

            return user;
        }

        public async Task<ApplicationUser> LoginByEmail(string email, string password)
        {
            var saltedPassword = GetSaltedPassword(password);

            var user = await GetUserByEmail(email);

            if (user == null) throw new Exception("Email not found");
            if (!await _userManager.CheckPasswordAsync(user, saltedPassword)) throw new Exception("Invalid Password");

            return user;
        }

        public async Task<ApplicationUser> GetUserById(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            return user;
        }

        public async Task<ApplicationUser> GetUserByEmail(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            return user;
        }

        public async Task<ApplicationUser> GetUserByUsername(string username)
        {
            var user = await _userManager.FindByNameAsync(username);

            return user;
        }

        public async Task<ApplicationUser> LoginByClaim(ClaimsPrincipal claim)
        {
            if (claim.Identity?.IsAuthenticated == false) return null;

            var userId = _userManager.GetUserId(claim);
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null) throw new Exception("User account not exist");

            return user;
        }

        public async Task<ApplicationUser> GetAuthenticatedUser(ClaimsPrincipal claim)
        {
            var user = await _userManager.GetUserAsync(claim);

            return user;
        }

        public async Task<ApplicationUser> Register(string email, string username, string password)
        {
            var saltedPassord = GetSaltedPassword(password);

            var existingEmail = await GetUserByEmail(email);
            var existingUserName = await GetUserByUsername(username);

            var user = new ApplicationUser
            {
                Email = email,
                UserName = username,
            };

            var result = await _userManager.CreateAsync(user, saltedPassord);
            if (!result.Succeeded)
            {
                var errorMessage = result.Errors.FirstOrDefault();
                throw new Exception((errorMessage?.Code + "-" + errorMessage?.Description) ?? "-");
            }

            return user;
        }

        public async Task<IEnumerable<string>> GetRoles(ApplicationUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);

            return roles;
        }

        public async Task<bool> CheckPolicy(ClaimsPrincipal user, string policy)
        {
            var checkPolicy = await _authorizationService.AuthorizeAsync(user, policy);
            if (checkPolicy.Succeeded) return true;

            return false;
        }

        public async Task<string> GetAccessToken(ApplicationUser user)
        {
            var payload = new Dictionary<string, string>
            {
                { "sub", user.Id.ToString() },
                { "email", user.Email },
                { "username", user.UserName },
                { "roles", string.Join(";", await _userManager.GetRolesAsync(user)) }
            };

            return GetToken(payload, _jwtSettings);
        }

        public async Task<string> GetIdToken(ApplicationUser user)
        {
            var payload = new Dictionary<string, string>
            {
                { "id", user.Id.ToString() },
                { "email", user.Email },
                { "username", user.UserName }
            };

            return GetToken(payload, _jwtSettings);
        }

        private string GetToken(Dictionary<string, string> payload, JwtSettings options)
        {
            var claims = payload.Select(x => {
                return new Claim(x.Key, x.Value);
            }).ToList();

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.SigningKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                options.Issuer,
                options.Audience,
                claims,
                DateTime.Now,
                DateTime.Now.AddDays(7),
                creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public string GetSaltedPassword(string password)
        {
            return _passwordHashSalt + password;
        }

        private static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;

            return Math.Floor(diff.TotalSeconds);
        }
    }
}
