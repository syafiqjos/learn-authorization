using AuthorizationTest.Configurations;
using AuthorizationTest.Database;
using AuthorizationTest.Models;
using AuthorizationTest.Services;
using AuthorizationTest.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace AuthorizationTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(option =>
            {
                option.EnableAnnotations();
                option.SwaggerDoc("v1", new OpenApiInfo { Title = "AuthorizationTest", Version = "v1" });
                option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter a valid token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });
                option.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type=ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });
            });

            // builder.Services.AddAutoMapper(typeof());
            var mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ApplicationUser, UserViewModel>()
                    .ForMember(dest => dest.IsianUsername, act => act.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.IsianEmail, act => act.MapFrom(src => src.Email));
            }));

            builder.Services.AddDbContext<ApplicationDbContext>(option =>
            {
                option.UseNpgsql("Host=localhost;Database=authorization_test;Username=postgres;Password=mashudahP");
            });

            builder.Services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_.";

                options.Password.RequiredLength = 8;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequiredUniqueChars = 1;
            })
            .AddDefaultTokenProviders()
            .AddEntityFrameworkStores<ApplicationDbContext>();

            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Bearer";
                options.DefaultChallengeScheme = "Bearer";
                options.DefaultScheme = "Bearer";
            }).AddJwtBearer(options =>
            {
                options.IncludeErrorDetails = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ValidAlgorithms = ["HS256"],

                    ValidIssuer = "auth",
                    ValidAudience = "auth",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("2b99ed46-cf9e-4795-a4a5-0b26cc15e3f3"))
                };
            });

            builder.Services.AddScoped<AuthService, AuthService>(c =>
            {
                var jwtAuthConfig = new JwtSettings
                {
                    Audience = "auth",
                    Issuer = "auth",
                    SigningKey = "2b99ed46-cf9e-4795-a4a5-0b26cc15e3f3"
                };

                return new AuthService(
                    c.GetService<ApplicationDbContext>()!,
                    c.GetService<SignInManager<ApplicationUser>>()!,
                    c.GetService<UserManager<ApplicationUser>>()!,
                    c.GetService<RoleManager<ApplicationUser>>()!,
                    c.GetService<IAuthorizationService>()!,
                    jwtAuthConfig,

                    passwordHashSalt: "r@nd0mSalT"
                );
            });

            builder.Services.AddAuthorization(options =>
            {
                RegisterPolicies(options);
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseAuthorization();


            app.MapControllers();

            RegisterRoles(app).GetAwaiter().GetResult();

            app.Run();
        }

        private static void RegisterPolicies(AuthorizationOptions options)
        {
            var claims = new string[] {
                    "project.view", "project.create", "project.edit", "project.delete",
                    "post.view", "post.create", "post.edit", "post.delete"
                };

            foreach (var claim in claims)
            {
                options.AddPolicy(claim, policy => policy.RequireClaim(claim));
            }
        }

        private static async Task RegisterRoles(WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

                var admin = await userManager.FindByNameAsync("admin");
                if (admin == null)
                {
                    var result = await userManager.CreateAsync(new ApplicationUser { UserName = "admin", Email = "admin@bff.com" }, "Aaa123???");
                    
                    admin = await userManager.FindByNameAsync("admin");
                }

                var roles = new string[] { "admin", "editor", "member", "guest" };

                foreach (var role in roles)
                {
                    if (!await roleManager.RoleExistsAsync(role)) {
                        await roleManager.CreateAsync(new ApplicationRole { Name = role });
                    }
                }

                var claims = new string[] {
                    "project.view", "project.create", "project.edit", "project.delete",
                    "post.view", "post.create", "post.edit", "post.delete"
                };

                var adminRole = await roleManager.FindByNameAsync("admin");

                var existedClaims = await roleManager.GetClaimsAsync(adminRole);

                foreach (var claim in claims)
                {
                    if (existedClaims.FirstOrDefault(x => x.Type == "permission" && x.Value == claim) == null)
                    {
                        await roleManager.AddClaimAsync(adminRole, new Claim("permission", claim));
                    }
                }

                await userManager.AddToRoleAsync(admin, "admin");
            }
        }
    }
}
