using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        [Route("/Project/Create")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "project.create")]
        public IActionResult CreateProject()
        {
            return Ok("Project created");
        }

        [HttpPost]
        [Route("/Project/Edit")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "project.edit")]
        public IActionResult EditProject()
        {
            return Ok("Project edited");
        }

        [HttpPost]
        [Route("/Project/Delete")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "project.delete")]
        public IActionResult DeleteProject()
        {
            return Ok("Project deleted");
        }

        [HttpPost]
        [Route("/Book/Create")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "book.create")]
        public IActionResult CreateBook()
        {
            return Ok("Book created");
        }
    }
}
