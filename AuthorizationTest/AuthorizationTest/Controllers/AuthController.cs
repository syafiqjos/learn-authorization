﻿using AuthorizationTest.Requests;
using AuthorizationTest.Responses;
using AuthorizationTest.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace AuthorizationTest.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class AuthController : Controller
    {
        private readonly AuthService _authService;

        public AuthController(AuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [AllowAnonymous]
        [SwaggerOperation(Summary = "Register New User")]
        public async Task<IActionResult> Register([FromBody] RequestAuthRegister request)
        {
            try
            {
                var user = await _authService.Register(request.Email, request.Username, request.Password);
                var login = await _authService.SignIn(user);

                if (login == null)
                {
                    return UnprocessableEntity(JsonResponse.Error("Something went wrong"));
                }

                return Ok(JsonResponse.Success("Register success", new
                {
                    access_token = await _authService.GetAccessToken(user),
                    id_token = await _authService.GetIdToken(user)
                }));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, JsonResponse.UnknownError(ex));
            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [AllowAnonymous]
        [SwaggerOperation(Summary = "Login with email address")]
        public async Task<IActionResult> LoginByEmail([FromBody] RequestAuthLoginByEmail request)
        {
            try
            {
                var user = await _authService.LoginByEmail(request.Email, request.Password);
                var login = await _authService.SignIn(user);

                if (login == null)
                {
                    return Unauthorized(JsonResponse.Error("Something went wrong"));
                }

                return Ok(JsonResponse.Success("Login success", new
                {
                    access_token = await _authService.GetAccessToken(user),
                    id_token = await _authService.GetIdToken(user)
                }));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, JsonResponse.UnknownError(ex));
            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [SwaggerOperation(Summary = "Refresh current user token by Bearer token")]
        public async Task<IActionResult> LoginByAccessToken()
        {
            try
            {
                var user = await _authService.LoginByClaim(User);
                var login = await _authService.SignIn(user);

                if (user == null)
                {
                    return Unauthorized(JsonResponse.Error("Invalid access token"));
                }

                if (login == null)
                {
                    return Unauthorized(JsonResponse.Error("Something went wrong"));
                }

                return Ok(JsonResponse.Success("Login success", new
                {
                    access_token = await _authService.GetAccessToken(user),
                    id_token = await _authService.GetIdToken(user)
                }));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, JsonResponse.UnknownError(ex));
            }
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [SwaggerOperation(Summary = "Get Authorized Roles of current user")]
        public async Task<IActionResult> GetRoles()
        {
            try
            {
                var user = await _authService.GetAuthenticatedUser(User);
                var roles = await _authService.GetRoles(user);

                return Ok(JsonResponse.Success("Roles retrieved", new
                {
                    roles = roles
                }));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, JsonResponse.UnknownError(ex));
            }
        }
    }
}
